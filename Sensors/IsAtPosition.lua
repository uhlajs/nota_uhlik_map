local sensorInfo = {
	name = "IsAtPosition",
	desc = "Return true if unit is in cylinder with `center` and `radius`.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetUnitPosition = Spring.GetUnitPosition
local GetUnitTransporter = Spring.GetUnitTransporter

return function(unitID, center, radius)
  local radius = radius or 100
  local x, y, z = GetUnitPosition(unitID)
  return center:Distance(Vec3(x, y, z)) <= radius and GetUnitTransporter(unitID) == nil
end