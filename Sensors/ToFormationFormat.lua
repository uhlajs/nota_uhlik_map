local sensorInfo = {
	name = "ToFormationFormat",
	desc = "Create suitable format for commands in `formation` package.",
	author = "uhlajs",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

--- Create suitable format for commands in `formation` package.
-- @param troops [UnitID]: array of units
return function(troops)
  local ret = {}
  for i = 1, #troops do
    ret[troops[i]] = i
  end
  return ret
end