local sensorInfo = {
	name = "GetSafePoints",
	desc = "Return points which are out of range of enemy team.",
	author = "PatrikValkovic, uhlajs",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetEnemyUnitsPositions = Sensors.GetEnemyUnitsPositions
local GetGroundHeight = Spring.GetGroundHeight

-- @description 
return function(granularity, maxEnemyRange)
  local maxEnemyRange = maxEnemyRange or 1000
	local safePoints = {}
	local enemyPositions = GetEnemyUnitsPositions()

	for x = 0,  Game.mapSizeX, granularity do
		for z = 0,  Game.mapSizeZ, granularity do
			local point = Vec3(x, GetGroundHeight(x, z), z)
			local safe = true

			for _, enemyPosition in pairs(enemyPositions) do
				if point:Distance(enemyPosition) <= maxEnemyRange then
					safe = false
        end
			end
			
			if safe then
				safePoints[#safePoints + 1] = point
			end
		end
	end
	return safePoints
end