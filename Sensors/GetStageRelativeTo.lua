local sensorInfo = {
	name = "GetStageRelativeTo",
	desc = "From given stage and offset return new stage.",
	author = "uhlajs",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

--- From given stage and offset return new stage.
-- @param lineInfo table: line description
-- @param stageID int: stageID
-- @param offset int: shift of the battle line
return function(lineInfo, stageID, offset)
  local index = math.min(math.max(1, stageID + offset), #lineInfo.stages)
  return {id = index, point=lineInfo.stages[index]}
end