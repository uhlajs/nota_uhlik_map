local sensorInfo = {
	name = "GetCurrentStage",
	desc = "Return closest stage point for given line.",
	author = "uhlajs",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetUnitPosition = Spring.GetUnitPosition

local function getClosestStages(lineInfo, troops)
  local closestStages = {}

  for i = 1, #troops do
    local x, y, z = GetUnitPosition(troops[i])
    local currentPosition = Vec3(x, y, z)
    closestStages[i] = 1 -- by default first stage
    local smallestDistance = (currentPosition - lineInfo.stages[1]):Length()
    for j = 2, #lineInfo.stages do
      local currectDistance = (currentPosition - lineInfo.stages[j]):Length()
      if smallestDistance > currectDistance then
        closestStages[i] = j
        smallestDistance = currectDistance
      end
    end
  end

  return closestStages
end

local function minimalStage(lineInfo, closestStages)
  local minimalID = math.huge
  for _, stageID in ipairs(closestStages) do
    if minimalID > stageID then
        minimalID = stageID
    end
  end
  return {id=minimalID, point=lineInfo.stages[minimalID]}
end

local function maximalStage(lineInfo, closestStages)
  local maximalID = -math.huge
  for _, stageID in ipairs(closestStages) do
    if maximalID < stageID then
      maximalID = stageID
    end
  end
  return {id=maximalID, point=lineInfo.stages[maximalID]}
end

local function majorityStage(lineInfo, closestStages)
   -- Number of occurance
 local stageCounts = {}
 for _, stageID in ipairs(closestStages) do
   if stageCounts[stageID] == nil then
     stageCounts[stageID] = 1
   else
     stageCounts[stageID] = stageCounts[stageID] + 1
   end
 end

 -- Find stage which is closest to all units
 local bestStageID = 1
 local maxVotes = 0
 for key, value in pairs(stageCounts) do
   if value > maxVotes then
     bestStageID = key
     maxVotes = value
   end
 end

 return {id = bestStageID, point = lineInfo.stages[bestStageID]}
end

--- Return closest stage point for given line.
-- @param lineInfo table: line description
-- @param troops [unitID]: array of units
return function(lineInfo, troops, system)
  local system = system or "majority"
  local closestStages = getClosestStages(lineInfo, troops)

  if system == "majority" then
    return majorityStage(lineInfo, closestStages)
  elseif system == "minimal" then
    return minimalStage(lineInfo, closestStages)
  elseif system == "maximal" then
    return maximalStage(lineInfo, closestStages)
  end
end
