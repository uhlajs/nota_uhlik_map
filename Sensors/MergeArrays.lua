local sensorInfo = {
	name = "MergeArrays",
	desc = "Merge multiple arrays into one and remove duplicities.",
	author = "uhlajs",
	date = "2020-05-23",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- no cachining

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(arrays)
  local newArray = {}
  local hash = {}
  for _, array in ipairs(arrays) do
    for _, v in ipairs(array) do 
      if not hash[v] then
        newArray[#newArray + 1] = v
        hash[v] = true
      end
    end
  end
  return newArray
end
