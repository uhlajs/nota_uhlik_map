local sensorInfo = {
	name = "GetCanyonPoints",
	desc = "Return points in given altitude.",
	author = "uhlajs",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = math.huge
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetGroundHeight = Spring.GetGroundHeight

return function(granularity, minHeight, maxHeight)
	local canyonPoints = {}
	for x = 0,  Game.mapSizeX, granularity do
		for z = 0,  Game.mapSizeZ, granularity do
			local y = GetGroundHeight(x, z)
      if y >= minHeight and y < maxHeight then
        canyonPoints[#canyonPoints + 1] = Vec3(x, y, z)
			end
		end
	end
	return canyonPoints
end