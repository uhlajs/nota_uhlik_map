local sensorInfo = {
	name = "GetUnitsPositions",
	desc = "Return units positions.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetUnitPosition = Spring.GetUnitPosition

-- @description return current units positions
return function(unitsList)
	local positions = {}
	for i = 1, #unitsList do
		local x, y, z = GetUnitPosition(unitsList[i])
		positions[i] = Vec3(x, y, z)
	end
	return positions
end