local sensorInfo = {
	name = "CreateGridFromPoints",
	desc = "Creates grid of points where every point is stored as key with his neighbours (in range) as values",
	author = "uhlajs",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local function getPointsTable(points)
	local pointsTable = {}

	local x2index = {}
	table.sort(points, function(left, right) return left.x < right.x end)
	for _, point in ipairs(points) do
		if x2index[point.x] == nil then
			local index = #pointsTable + 1
			x2index[point.x] = index
			pointsTable[index] = {x=point.x, array={}, z2index={}}
		end
	end

	table.sort(points, function(left, right) return left.z < right.z end)
	for _, point in ipairs(points) do
		local array = pointsTable[x2index[point.x]].array
		local z2index = pointsTable[x2index[point.x]].z2index
		if z2index[point.z] == nil then
			local index = #array + 1
			z2index[point.z] = index
			array[index] = {z=point.z, points={point}}
		else
			local ps = array[z2index[point.z]].points
			ps[#ps + 1] = point
		end
	end

	return pointsTable, x2index
end

local function getCandidates(pointsTable, xIndex, point)
	local candidates = {}

	-- Going around the x axis list
	for _, j in ipairs({xIndex - 1, xIndex, xIndex + 1}) do
		-- Check validity
		if 0 < j and j <= #pointsTable then
			local array = pointsTable[j].array
			local z2index = pointsTable[j].z2index
			local zIndex = z2index[point.z]

			if zIndex ~= nil then
				-- Going around z axis list
				for _, i in ipairs({zIndex - 1, zIndex, zIndex + 1}) do
					-- Check validity
					if 0 < i and i <= #array then
						-- Append all points to candidates -> max 3*3*z_points
						for _, p in ipairs(array[i].points) do
							candidates[#candidates + 1] = p
						end
					end
				end
			end
		end
	end

	return candidates
end

return function(points, range)
	local pointsTable, x2index = getPointsTable(points)
	local grid = {}

	for a, point in ipairs(points) do
		local candidates = getCandidates(pointsTable, x2index[point.x], point)
		grid[point] = {}

		-- Iterate over all candidates
		for _, candidate in ipairs(candidates) do
			local dist = -1
			if point ~= candidate then
				dist = point:Distance(candidate)
			end
			if dist ~= -1 and 0 < dist and dist <= range then
				-- Save the candidate to list
				local index = #grid[point] + 1
				grid[point][index] = candidate
			end
		end
	end

	return grid
end
