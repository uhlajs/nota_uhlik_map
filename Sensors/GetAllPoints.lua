local sensorInfo = {
	name = "GetAllPoints",
	desc = "Return array with all points wi given granularity.",
	author = "uhlajs",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = math.huge

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetGroundHeight = Spring.GetGroundHeight
 
return function(granularity)
	local points = {}
	for x = 0,  Game.mapSizeX, granularity do
		for z = 0,  Game.mapSizeZ, granularity do
      points[#points + 1] = Vec3(x, GetGroundHeight(x, z), z)
		end
	end
	return points
end