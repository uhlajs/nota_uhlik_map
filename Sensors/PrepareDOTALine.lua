local sensorInfo = {
	name = "PrepareDOTALine",
	desc = "Prepare DOTA line.",
	author = "uhlajs",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

--- Prepare meta information about line
-- @param originalPoints [Vec3]: array of points describing line
-- @param stepSize int: granuarity between two subsequent points
-- @ return {stepSize int, stages [Vec3]}: table where stages is array of points with grauarity `stepSize`
return function(originalPoints, stepSize)
  local lineInfo = {
    stepSize = stepSize or 200,
    stages = {},
  }

  local start = originalPoints[1]
  for i = 2, #originalPoints do
    local target = originalPoints[i]
    local normalizedDirection = (target - start):GetNormal()
    local numberOfSegments = math.floor((target - start):Length() / lineInfo.stepSize)
    for j = 0, numberOfSegments do
      local stage = start + normalizedDirection * j * lineInfo.stepSize
      if #lineInfo.stages == 0 or (stage - lineInfo.stages[#lineInfo.stages]):Length() / 2 > lineInfo.stepSize then
        lineInfo.stages[#lineInfo.stages + 1] = stage
      end
    end
    start = target
  end
  -- Add last point
  lineInfo.stages[#lineInfo.stages + 1] = originalPoints[#originalPoints]

  return lineInfo
end