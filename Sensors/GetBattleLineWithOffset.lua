local sensorInfo = {
	name = "GetBattleLineWithOffset",
	desc = "Return first line stage where is an enemy unit.",
	author = "uhlajs",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetUnitsInSphere = Spring.GetUnitsInSphere
local EnemyTeamIDs = Sensors.core.EnemyTeamIDs
local GetStageRelativeTo = Sensors.GetStageRelativeTo

--- Return first line stage where is an enemy unit.
-- @param lineInfo table: line description
-- @param offset int: shift of the battle line
-- @param radius int: radius in which are units considered to be in given line stage
return function(lineInfo, offset, radius)
  local offset = offset or 0
  local radius = radius or 2 * lineInfo.stepSize
  local enemyIDs = EnemyTeamIDs()
  -- TODO: Remove this otherwise neutral units are considered as enemy
  enemyIDs[#enemyIDs + 1] = 1

  for i, stage in ipairs(lineInfo.stages) do
    for _, enemyTeamID in ipairs(enemyIDs) do
      local units = GetUnitsInSphere(stage.x, stage.y, stage.z, radius, enemyTeamID)
      if #units ~= 0 then
        return GetStageRelativeTo(lineInfo, i, offset)
      end
    end
  end
  -- By default rush to the end
  return {id=#lineInfo.stages, point=lineInfo.stages[#lineInfo.stages]}
end