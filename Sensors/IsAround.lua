local sensorInfo = {
	name = "IsAround",
	desc = "Chech whether there is a dead unit around.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetFeaturesInSphere = Spring.GetFeaturesInSphere
local GetUnitsInSphere = Spring.GetUnitsInSphere
local EnemyTeamIDs = Sensors.core.EnemyTeamIDs
local GetUnitPosition = Spring.GetUnitPosition

return function(who, troops, radius)
	for _, unitID in ipairs(troops) do
		local x, y, z = GetUnitPosition(unitID)
		local center = Vec3(x, y, z)
		local targets = {}

		if who == "feature" then
			targets = GetFeaturesInSphere(center.x, center.y, center.z, radius)
		elseif who == "enemy" then
			local enemyIDs = EnemyTeamIDs()
			  -- TODO: Remove this otherwise neutral units are considered as enemy
			enemyIDs[#enemyIDs + 1] = 1
			for _, enemyTeamID in ipairs(enemyIDs) do
				local enemyUnits = GetUnitsInSphere(center.x, center.y, center.z, radius, enemyTeamID)
				for _, enemyID in ipairs(enemyUnits) do
					targets[#targets + 1] = enemyID
				end
			end
		end

		if #targets ~= 0 then
			return true
		end
	end
  return false
end