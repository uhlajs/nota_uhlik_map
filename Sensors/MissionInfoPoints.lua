local sensorInfo = {
	name = "MissionInfoPoints.",
	desc = "Get Mission info points.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local MissionInfo = Sensors.core.MissionInfo

local treshold = 0

return function()
  local missionInfo = MissionInfo()
  local points = {
    top = {},
    mid = {},
    bot = {},
  }

  -- Top
  for i, point in ipairs(missionInfo.corridors.Top.points) do
    if i >= treshold then
      points.top[#points.top + 1] = point.position
    end
  end
  -- Mid
  for i, point in ipairs(missionInfo.corridors.Middle.points) do
    if i >= treshold then
      points.mid[#points.mid + 1] = point.position
    end
  end
  -- Bot
  for i, point in ipairs(missionInfo.corridors.Bottom.points) do
    if i >= treshold then
      points.bot[#points.bot + 1] = point.position
    end
  end
  return points
end