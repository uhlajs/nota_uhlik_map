local sensorInfo = {
	name = "GetNextStage",
	desc = "Return next stage on the given line.",
	author = "uhlajs",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetUnitPosition = Spring.GetUnitPosition
local GetBattleLineWithOffset = Sensors.GetBattleLineWithOffset
local GetCurrentStage = Sensors.GetCurrentStage
local GetStageRelativeTo = Sensors.GetStageRelativeTo

--- Return next stage on the given line.
-- @param lineInfo table: line description
-- @param troops [unitID]: array of units
-- @param offset int: shift of the battle line
return function(lineInfo, troops, offset)
  local battleLine = GetBattleLineWithOffset(lineInfo, offset)
  local currentStage = GetCurrentStage(lineInfo, troops, "majority")

  if battleLine.id > currentStage.id then
    --Spring.Echo("FORWARD")
    return GetStageRelativeTo(lineInfo, currentStage.id, 1)
  elseif battleLine.id < currentStage.id then
    --Spring.Echo("BACKWARD")
    return GetStageRelativeTo(lineInfo, currentStage.id, -1)
  else
    --Spring.Echo("STAY")
    return battleLine
  end
end