local sensorInfo = {
	name = "GetEnemyUnitsPositions",
	desc = "Returns positions of the all enemy units.",
	author = "uhlajs",
	date = "2020-05-23",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local EnemyUnits = Sensors.core.EnemyUnits
local GetUnitPosition = Spring.GetUnitPosition

return function()
  local enemyUnits = EnemyUnits()
  local enemyPositions = {}
  for _, unitID in pairs(enemyUnits) do
    local x, y, z = GetUnitPosition(unitID)
    enemyPositions[#enemyPositions + 1] = Vec3(x, y, z)
  end
  return enemyPositions
end
