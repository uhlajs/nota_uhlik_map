function getInfo()
  return {
      onNoUnits = SUCCESS,
      tooltip = "",
      parameterDefs = {
          {
              name = "unitID",
              variableType = "expression",
              componentType = "editBox",
              defaultValue = "unitID",
          },
          {
              name = "point",
              variableType = "expression",
              componentType = "editBox",
              defaultValue = "point",
          },
          {
            name = "treshold",
            variableType = "expression",
            componentType = "editBox",
            defaultValue = "50",
        },
      }
  }
end

local function ClearState(self)
  self.initialization = false
end

local GetUnitPosition = Spring.GetUnitPosition
local ValidUnitID = Spring.ValidUnitID

local function distance(a, b)
	return math.sqrt(math.pow(a.x - b.x, 2) + math.pow(a.z - b.z, 2))
end

local function checkFail(self, parameter)
  return not ValidUnitID(parameter.unitID)
end

local function checkSuccess(self, parameter)
  local x, y, z = GetUnitPosition(parameter.unitID)
  return distance(Vec3(x, y, z), parameter.point) < parameter.treshold
end

function Run(self, units, parameter)
  if checkFail(self, parameter) then
      return FAILURE
  elseif checkSuccess(self, parameter) then
      return SUCCESS
  end

  if not self.initialization then
      self.initialization = true
      Spring.GiveOrderToUnit(
          parameter.unitID,
          CMD.MOVE,
          parameter.point:AsSpringVector(),
          {}
      )
  end
  return RUNNING
end

function Reset(self)
  ClearState(self)
end