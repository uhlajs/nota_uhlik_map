function getInfo()
  return {
      onNoUnits = SUCCESS,
      tooltip = "",
      parameterDefs = {
          {
              name = "unitID",
              variableType = "expression",
              componentType = "editBox",
              defaultValue = "unitID",
          },
          {
              name = "vec",
              variableType = "expression",
              componentType = "editBox",
              defaultValue = "vec",
          },
          {
            name = "treshold",
            variableType = "expression",
            componentType = "editBox",
            defaultValue = "50",
        },
      }
  }
end

local function ClearState(self)
  self.initialization = false
  self.target = nil
end

local GetUnitPosition = Spring.GetUnitPosition
local ValidUnitID = Spring.ValidUnitID

local function distance(a, b)
	return math.sqrt(math.pow(a.x - b.x, 2) + math.pow(a.z - b.z, 2))
end

local function checkFail(self, parameter)
  return not ValidUnitID(parameter.unitID)
end

local function checkSuccess(self, parameter)
  if self.target == nil then
    return false
  end
  local x, y, z = GetUnitPosition(parameter.unitID)
  return distance(Vec3(x, y, z), self.target) < parameter.treshold
end

function Run(self, units, parameter)
  if checkFail(self, parameter) then
      return FAILURE
  elseif checkSuccess(self, parameter) then
      return SUCCESS
  end

  if not self.initialization then
      self.initialization = true
      local x, y, z = GetUnitPosition(parameter.unitID)
      self.target = parameter.vec + Vec3(x,y,z)
      Spring.GiveOrderToUnit(
          parameter.unitID,
          CMD.MOVE,
          (self.target):AsSpringVector(),
          {}
      )
  end
  return RUNNING
end

function Reset(self)
  ClearState(self)
end