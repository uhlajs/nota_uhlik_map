function getInfo()
  return {
      onNoUnits = FAILURE,
      tooltip = "Move unit along given path.",
      parameterDefs = {
          {
              name = "unitID",
              variableType = "expression",
              componentType = "editBox",
              defaultValue = "unitID",
          },
          {
              name = "path",
              variableType = "expression",
              componentType = "editBox",
              defaultValue = "path",
          },
          {
            name = "treshold",
            variableType = "expression",
            componentType = "editBox",
            defaultValue = "200",
        },
      }
  }
end

local GetUnitPosition = Spring.GetUnitPosition
local ValidUnitID = Spring.ValidUnitID

local lastPosition

local function ClearState(self)
  self.initialization = false
end

local function distance(a, b)
	return math.sqrt(math.pow(a.x - b.x, 2) + math.pow(a.z - b.z, 2))
end

local function checkFail(self, parameter)
  return not ValidUnitID(parameter.unitID)
--[[   if not ValidUnitID(parameter.unitID) then 
    return true
  end
  local x, y, z = GetUnitPosition(parameter.unitID)
  currPosition = Vec3(x, y, z)
  if lastPosition == nil then
    lastPosition = currPosition
    return false
  elseif (lastPosition - currPosition):Distance() < 10 then
    lastPosition = nil
    return true
  end
  return false ]]
end

local function checkSuccess(self, parameter)
  local x, y, z = GetUnitPosition(parameter.unitID)
  return distance(parameter.path[#parameter.path], Vec3(x, y, z)) < parameter.treshold
end

function Run(self, units, parameter)
  if checkFail(self, parameter) then
      return FAILURE
  elseif checkSuccess(self, parameter) then
      return SUCCESS
  end

  if not self.initialization then
      self.initialization = true
      for _, point in ipairs(parameter.path) do
        Spring.GiveOrderToUnit(
            parameter.unitID,
            CMD.MOVE,
            point:AsSpringVector(), 
            {"shift"}
        )
      end
  end
  return RUNNING
end

function Reset(self)
  ClearState(self)
end