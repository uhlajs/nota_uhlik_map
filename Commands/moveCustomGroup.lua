function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "groupDefintion",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter groupDefintion [table] - mapping unitID => positionIndex
			--[[ local example = {
				[14945] = 1,
				[5814] = 2,
				[126450] = 3,
			}
			]]--
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "formation", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<relative formation>",
			},
			-- @parameter formation [array] - list of Vec3
			--[[ local example = {
				[1] = Vec3(0,0,0),
				[2] = Vec3(10,0,0),
				[3] = Vec3(-10,0,0),
			}
			]]--
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			},
			{
				name = "treshold",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "50",
		}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local ValidUnitID = Spring.ValidUnitID

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local customGroup = parameter.groupDefintion -- table
	local position = parameter.position -- Vec3
	local formation = parameter.formation -- array of Vec3
	local fight = parameter.fight -- boolean
	
	--Spring.Echo(dump(parameter.formation))
	
	-- validation
	-- if (#units > #formation) then
		-- Logger.warn("formation.move", "Your formation size [" .. #formation .. "] is smaller than needed for given count of units [" .. #units .. "] in this group.") 
		-- return FAILURE
	-- end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	-- choose the pointmen -> one closes to the target
	--[[ local pointmanID
	local minDist = math.huge
	for unitID, posIndex in pairs(customGroup) do
		if not ValidUnitID(unitID) then
			return FAILURE
		end
		local targetPosition = position + formation[posIndex]
		local x, y, z = SpringGetUnitPosition(unitID)
		local unitPosition = Vec3(x, y, z)
		local dist = unitPosition:Distance(targetPosition)
		if minDist > dist then
			minDist = dist
			pointmanID = unitID
		end
	end ]]
	local minIndex = math.huge
	local pointmanID
	local pivotmanIndex
	for unitID, posIndex in pairs(customGroup) do
		if pointmanID == nil then
			pointmanID = unitID
			pivotmanIndex = posIndex
		end
		if bb.troopsInfoMap[unitID] ~= nil and minIndex > bb.troopsInfoMap[unitID].id then
			minIndex = bb.troopsInfoMap[unitID].id 
			pointmanID = unitID
			pivotmanIndex = posIndex
		end
	end

	if not ValidUnitID(pointmanID) then
		return FAILURE
	end

	local pointX, pointY, pointZ = SpringGetUnitPosition(pointmanID)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	
	-- threshold of pointan success
	if (pointmanPosition == self.lastPointmanPosition) then 
		self.threshold = self.threshold + THRESHOLD_STEP 
	else
		self.threshold = THRESHOLD_DEFAULT
	end
	self.lastPointmanPosition = pointmanPosition
	
	-- check pointman success
	-- THIS LOGIC IS TEMPORARY, NOT CONSIDERING OTHER UNITS POSITION
	local pointmanOffset = formation[pivotmanIndex]
	local pointmanWantedPosition = position + pointmanOffset
	if (pointmanPosition:Distance(pointmanWantedPosition) - parameter.treshold < self.threshold) then
		return SUCCESS
	else
		SpringGiveOrderToUnit(pointmanID, cmdID, pointmanWantedPosition:AsSpringVector(), {})
		
		for unitID, posIndex in pairs(customGroup) do
			if unitID ~= pointmanID then
				bb.test = formation[posIndex]
				local thisUnitWantedPosition = pointmanPosition - pointmanOffset + formation[posIndex]
				if fight then
					SpringGiveOrderToUnit(unitID, CMD.FIRE_STATE, {3}, {})
				end
				SpringGiveOrderToUnit(unitID, cmdID, thisUnitWantedPosition:AsSpringVector(), {})
			end
		end
		
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
