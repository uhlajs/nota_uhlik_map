# NOTA Map

Author: **Jan Uhlík (uhlajs)**

## Credits

All icons are taken from [Icons8](https://icons8.com) and are distributed under a [Creative Commons Attribution-NoDerivs 3.0 Unported](https://creativecommons.org/licenses/by-nd/3.0/) license.

## Usage

You have to copy all widgets from `Widgets` folder into `000/SpringData/LuaUI/Widgets` and allowed them afterward in `F11` menu.

## Ideas

Fix `moveCustomGroup.lua` for finding best suitable `pointman`.